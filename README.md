![Docker Pulls](https://img.shields.io/docker/pulls/psychomantys/jenkins)

# Jenkins imagem with a bunch of plugins

This README would normally document whatever steps are necessary to get your application up and running.

* configuration-as-code
* blueocean
* docker-workflow
* jclouds-jenkins
* token-macro
* durable-task
* docker-plugin
* jdk-tool
* ssh-slaves
* ldap
* github
* bitbucket
* ws-cleanup
* timestamper
* ssh-agent
* sonar
* slack
* role-strategy
* rebuild
* publish-over-ssh
* pipeline-github-lib
* pipeline-utility-steps
* workflow-aggregator
* parameterized-trigger
* antisamy-markup-formatter
* blueocean-jira
* jira
* gitlab-plugin
* git-parameter
* external-monitor-job
* email-ext
* command-launcher
* build-timeout
* multibranch-scan-webhook-trigger
* cloudbees-bitbucket-branch-source
* terraform
* pipeline-multibranch-defaults
* basic-branch-build-strategies
* parameterized-trigger
* generic-webhook-trigger
* matrix-auth
* aws-credentials
* job-dsl
* naginator
* http_request
* slack
* email-ext
* prometheus
* cobertura
* junit
* matrix-project
* ansicolor
* pipeline-graph-view
* embeddable-build-status
* join
* configuration-as-code-groovy
* pipeline-stage-view
* lockable-resources
* ansible
* ws-cleanup
* build-timeout
* kubernetes
* git
* workflow-scm-step
* pipeline-groovy-lib
* scm-api
