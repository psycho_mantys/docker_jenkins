# syntax = docker/dockerfile:1

FROM jenkins/jenkins:lts-jdk21

#ARG JENKINS_PLUGIN="configuration-as-code blueocean:1.24.7 docker-workflow:1.26"
ARG JENKINS_PLUGIN="configuration-as-code blueocean \
  docker-workflow jclouds-jenkins token-macro pipeline-multibranch-defaults \
  durable-task docker-plugin jdk-tool jclouds-jenkins terraform \
  ssh-slaves ldap github bitbucket cloudbees-bitbucket-branch-source \
  basic-branch-build-strategies parameterized-trigger generic-webhook-trigger \
  matrix-auth aws-credentials job-dsl naginator http_request slack ssh-agent \
  email-ext prometheus cobertura junit matrix-project ansicolor pipeline-graph-view \
  embeddable-build-status join configuration-as-code-groovy pipeline-stage-view \
  lockable-resources ansible ws-cleanup build-timeout kubernetes git \
  oidc-provider mercurial \
  workflow-scm-step pipeline-groovy-lib \
  workflow-cps workflow-step-api \
  cloudbees-folder variant \
  workflow-multibranch branch-api \
  pipeline-model-definition pipeline-github-lib \
  workflow-cps-global-lib-http \
  custom-folder-icon \
  scm-api kubernetes-credentials-provider"

ARG APT_EXTRA="\
  apt-transport-https ca-certificates \
  curl gnupg2 software-properties-common \
  bash openssh-client unzip pixz jq \
  dbus-user-session lsb-release \
  git gettext-base"

ENV JAVA_OPTS="-server -Xms900m -Xmx2048m -XX:MaxRAMPercentage=95.0 -XX:+UseParallelGC -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90"

USER root
RUN apt-get clean && apt-get --allow-unauthenticated --allow-insecure-repositories update

RUN apt-get --allow-unauthenticated --allow-insecure-repositories update && \
  apt-get --allow-unauthenticated install -y ${APT_EXTRA} && \
  curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
  apt-key fingerprint 0EBFCD88 && \
  add-apt-repository \
    "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" && \
  apt-get --allow-insecure-repositories update && \
  apt-get --allow-unauthenticated install -y docker-ce-cli && \
  rm -rf /usr/share/man /usr/share/doc && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

ARG CACHE_DIR="/tmp/cache_plugins/"
RUN --mount=type=cache,mode=0777,target=/tmp/cache_plugins \
  mkdir -p "${CACHE_DIR}" && chown jenkins.jenkins -Rv "${CACHE_DIR}"

USER jenkins
#ENV JENKINS_UC_DOWNLOAD="http://mirrors.jenkins-ci.org"
#ENV JENKINS_UC_DOWNLOAD="http://ftp-nyc.osuosl.org/pub/jenkins"
#RUN while jenkins-plugin-cli --plugins "${JENKINS_PLUGIN}" ; do sleep 1 ; done

ARG CURL_OPTIONS="-sSfLk --connect-timeout 60 --retry 6 --retry-delay 0 --retry-max-time 120"
RUN --mount=type=cache,mode=0777,target=/tmp/cache_plugins \
  echo "${JENKINS_PLUGIN}" \
  | tr ' ' '\n' \
  | xargs -n1 jenkins-plugin-cli --plugins

RUN mkdir -p ${HOME}/.config/docker/ \
  && echo '{"storage-driver": "fuse-overlayfs"}' > ${HOME}/.config/docker/daemon.json

